import React from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import Homepage from './pages/homepage';
import SecondPage from './pages/secondpage';

const App: React.FC = () => {
  return (
    <html>
      <h1>Hello React</h1>
        <Router>
          <Route exact path="/" component={Homepage}></Route>
          <Route path="/secondpage" component={SecondPage}></Route>
        </Router>

    </html>
  );
}

export default App;
